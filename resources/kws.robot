***Settings***
Documentation       Palavras chaves de automação dos comportamentos


***Keywords***
# Consultar a ficha de um super herói (/id)    
Quando requisitar o histórico do super herói "298"             
        ${REPLY_Green_Arrow}            Get Request             superheroAPI            ${ID_HERO.ID_Green_Arrow}
        Set Test Variable               ${REPLY_Green_Arrow}

Então deve ser retornado que a sua inteligência é superior a "${HERO_INTELLIGENCE}"
        Should Be True                  (${REPLY_Green_Arrow.json()["powerstats"]["intelligence"]} > ${HERO_INTELLIGENCE})

E deve ser retornado que o seu verdadeiro nome é ser "${FULL_NAME_HERO}"
        Should Be Equal As Strings      ${REPLY_Green_Arrow.json()["biography"]["full-name"]}    ${FULL_NAME_HERO}

E deve ser retornado que é afiliado do grupo "${GROUP_HERO}"
        Should Contain Any              ${REPLY_Green_Arrow.json()["connections"]["group-affiliation"]}    ${GROUP_HERO}

#Consultar qual o super herói mais inteligente, rápido e forte (/id/powerstats)
Quando requisitar as estatísticas de poder dos super heróis "Flash" e "Ant-Man"
        ${REPLY_Flash}                  Get Request             superheroAPI            ${ID_HERO.ID_Flash}/powerstats
        ${REPLY_Ant_Man}                Get Request             superheroAPI            ${ID_HERO.ID_Ant_Man}/powerstats
        Set Test Variable               ${REPLY_Flash}
        Set Test Variable               ${REPLY_Ant_Man}

Então deve ser retornado que o mais inteligente é o herói "Ant-Man"
        Should Be True                  (${REPLY_Ant_Man.json()["intelligence"]} > ${REPLY_Flash.json()["intelligence"]})

E deve ser retornado que o mais rápido é o herói "Flash"
        Should Be True                  (${REPLY_Flash.json()["speed"]} > ${REPLY_Ant_Man.json()["speed"]})

E deve ser retornado que o mais forte é o herói "Ant-Man"
        Should Be True                  (${REPLY_Ant_Man.json()["strength"]} > ${REPLY_Flash.json()["strength"]})